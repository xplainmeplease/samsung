#include "Client.h"
#include "Server.h"
#include "Utilities.h"
#include "CommandLineOptions.h"
#include "FilePacketSerializer.h"

int main(int32_t argc, char** argv)
{
    CommandLineOptions cmd;

    int32_t cmdInitResult = cmd.Init(argc, argv);
    if(cmdInitResult < 0)
    {
        std::cout << "Error while cmd options init.\n";
        return cmdInitResult;
    }

    if(cmd.GetExecutionMode() == ExecutionMode::Client)
    {
        if(!cmd.IsValid(true))
        {
            std::cout << "Incorrect args for client.\n";
            CommandLineOptions::PrintHelp();
            return -1;
        }
        Client client(cmd.GetIPv4(), cmd.GetPort());
        const auto clientInitResult = client.Init();
        if(clientInitResult == -1)
        {
            return clientInitResult;
        }

        const auto connectResult = client.Connect();
        if(connectResult == - 1)
        {
            return connectResult;
        }

        const auto sendResult = client.SendData(FilePacketSerializer::CreateFilePacket(cmd.GetFilepath()));
        if(sendResult == -1)
        {
            return sendResult;
        }

        return 0;
    }

    if(cmd.GetExecutionMode() == ExecutionMode::Server)
    {
        if(!cmd.IsValid(false))
        {
            std::cout << "Incorrect args for server.\n";
            CommandLineOptions::PrintHelp();
            return -1;
        }

        Server serverConnection(cmd.GetPort());

        const auto serverInitResult = serverConnection.Init();
        if(serverInitResult == -1)
        {
            return serverInitResult;
        }
        const auto serverRunResult = serverConnection.Run();
        if(serverRunResult == -1)
        {
            return serverRunResult;
        }

        return 0;
    }


    return 0;
}
