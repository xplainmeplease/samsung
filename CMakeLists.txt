cmake_minimum_required(VERSION 3.21)
project(samsung)

set(CMAKE_CXX_STANDARD 17)

include_directories(src)
include_directories(src/CommandLineOptions)
include_directories(src/FilePacketSerializer)
include_directories(src/Endpoints)

add_executable(samsung main.cpp
        src/CommandLineOptions/CommandLineOptions.cpp
        src/Endpoints/Server.cpp
        src/FilePacketSerializer/FilePacketSerializer.cpp
        src/Endpoints/Client.cpp)
