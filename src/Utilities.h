#ifndef SAMSUNG_UTILITIES_H
#define SAMSUNG_UTILITIES_H

#include <cstdint>
#include <iostream>


enum class ExecutionMode : uint8_t
{
    Client = 0,
    Server,
    Undefined
};

enum class OptionID : uint8_t
{
    ExecutionMode = 0,
    IPv4,
    Port,
    Filepath
};




#endif //SAMSUNG_UTILITIES_H
