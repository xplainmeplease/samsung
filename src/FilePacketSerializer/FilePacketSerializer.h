#ifndef SAMSUNG_FILEPACKETSERIALIZER_H
#define SAMSUNG_FILEPACKETSERIALIZER_H

#include <string>

class FilePacketSerializer
{
public:
    [[nodiscard]] static std::string CreateFilePacket(const std::string& filename) noexcept;

    [[nodiscard]] static bool FilePacketIsValid(const std::string& filePacket);

    [[nodiscard]] static std::string    GetFilename(const std::string& filePacket) noexcept;
};


#endif //SAMSUNG_FILEPACKETSERIALIZER_H
