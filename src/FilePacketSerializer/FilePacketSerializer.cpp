#include "FilePacketSerializer.h"

#include <filesystem>
#include <fstream>
#include <random>
#include <iostream>

std::string FilePacketSerializer::CreateFilePacket(const std::string & filename) noexcept
{
    std::string resultString;

    resultString += filename;
    resultString += "|";
    const auto currentSize = resultString.size();

    if(std::filesystem::exists(filename))
    {
        std::ifstream file(filename, std::ios::in | std::ios::binary);
        const auto fileSize = std::filesystem::file_size(filename);
        resultString.resize(currentSize + fileSize);
        file.read(resultString.data() + currentSize, resultString.size() - currentSize);
        return resultString;
    }

    std::ofstream randomizedFile(filename);
    std::random_device rd;
    std::mt19937 gen(rd());

    const auto random = [&gen](int low, int high)
    {
        std::uniform_int_distribution<> dist(low, high);
        return dist(gen);
    };

    const auto resultDataSize = random(0,1'000);
    resultString.resize(currentSize + resultDataSize);

    static const std::string symbols = "#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ";

    for(auto i = currentSize; i < resultString.size(); ++i)
    {
        resultString[i] = symbols[random(0,symbols.size())];
    }

    randomizedFile.write(resultString.c_str(),resultString.size());

    std::cout << "Result string to be sended: " << resultString << "\n";
    return resultString;
}

std::string FilePacketSerializer::GetFilename(const std::string & filePacket) noexcept
{
    return filePacket.substr(0,filePacket.find('|'));
}

bool FilePacketSerializer::FilePacketIsValid(const std::string & filePacket)
{
    if(filePacket.size() > 2)
    {
        const auto filenameEndIndex = filePacket.find('|');
        if(filenameEndIndex != std::string::npos)
        {
            if(filePacket.size() > filenameEndIndex + 1)
            {
                 return true;
            }
            return false;
        }
        return false;
    }
    return false;
}
