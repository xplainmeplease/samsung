#ifndef SAMSUNG_CLIENT_H
#define SAMSUNG_CLIENT_H

#include <string>
#include <unistd.h>

class Client
{
public:
    Client(std::string  ip, uint16_t port) noexcept;
    ~Client();

    Client(const Client& other) = delete;
    Client& operator=(const Client& other) = delete;

    Client(Client&& other) = delete;
    Client& operator=(Client&& other) = delete;

public:
    [[nodiscard]] int32_t Init() noexcept;
    [[nodiscard]] int32_t Connect() noexcept;
    [[nodiscard]] int32_t SendData(const std::string& data) const noexcept;
private:
    int32_t sockfd_;
    std::string ip_;
    uint16_t port_;
};


#endif //SAMSUNG_CLIENT_H
