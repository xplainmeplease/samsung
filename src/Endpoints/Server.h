#ifndef SAMSUNG_SERVER_H
#define SAMSUNG_SERVER_H

#include <fstream>
#include <unistd.h>

class Server
{
public:
    explicit Server(uint16_t port) noexcept;
    ~Server();

    Server(const Server& other) = delete;
    Server& operator=(const Server& other) = delete;

    Server(Server&& other) = delete;
    Server& operator=(Server&& other) = delete;

public:
    [[nodiscard]] int32_t Init() noexcept;
    [[nodiscard]] int32_t Run() noexcept;

private:
    int32_t BindSocket2Port() noexcept;

private:
    int32_t sockfd_;
    int32_t connectionSocket_;
    uint16_t port_;
    std::string buffer_;

};


#endif //SAMSUNG_SERVER_H
