#include "Client.h"

#include <utility>
#include <netinet/in.h>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>

Client::Client(std::string  ip, uint16_t port) noexcept
    : sockfd_{0}, ip_{std::move(ip)}, port_{port}
{
}

Client::~Client()
{
    shutdown(sockfd_, SHUT_WR);
    close(sockfd_);
}

int32_t Client::Connect() noexcept
{
    sockaddr_in serverAddr {};

    memset(reinterpret_cast<char*>(&serverAddr), 0, sizeof(serverAddr));

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(ip_.c_str());
    serverAddr.sin_port = htons(port_);

    const auto connectResult = connect(sockfd_, (sockaddr*)&serverAddr, sizeof(serverAddr));
    if (connectResult == -1)
    {
        std::cout << "Could not connect to server![" << ip_ << "]\n";
        std::cout << strerror(errno) << "\n";
        return connectResult;
    }
    std::cout << "Successfully connected\n";

    return connectResult;
}

int32_t Client::Init() noexcept
{
    sockfd_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockfd_ == -1)
    {
        std::cout << "Bad server socket init\n";
        return -1;
    }

    int32_t enable {1};
    if (setsockopt(sockfd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, sizeof(int)) < 0)
    {
        std::cout << "Bad server socket init\n";
        return -1;
    }

    return 0;
}

int32_t Client::SendData(const std::string & data) const noexcept
{
    uint64_t totalBytesSent {0u};

    auto sendResult = send(sockfd_,data.c_str(), data.size(),0);
    if(sendResult == -1)
    {
        std::cout << "Send error.\n";
    }

    std::cout << "Sent: " << sendResult <<" bytes.\n";


    return 0;
}

