#include <netinet/in.h>
#include <cstring>
#include <iostream>
#include <csignal>

#include "FilePacketSerializer.h"
#include "Server.h"

Server::Server(uint16_t port) noexcept
    : sockfd_{}, connectionSocket_{}, port_{port}, buffer_()
{
}

Server::~Server()
{
    shutdown(sockfd_, SHUT_WR);
    close(sockfd_);
}

int32_t Server::BindSocket2Port() noexcept
{
    int32_t result{0};

    sockaddr_in socketData {};

    sockfd_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockfd_ == -1)
    {
        std::cout << "Bad server socket init\n";
        return -1;
    }

    int32_t enable {1};
    if (setsockopt(sockfd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, sizeof(int)) < 0)
    {
        std::cout << "Bad server socket init\n";
        return -1;
    }

    memset(reinterpret_cast<char*>(&socketData),0,sizeof(socketData));

    socketData.sin_family = AF_INET;
    socketData.sin_addr.s_addr = INADDR_ANY;
    socketData.sin_port = htons(port_);

    signal(SIGPIPE, SIG_IGN);
    if(bind(sockfd_, reinterpret_cast<sockaddr*>(&socketData), sizeof(socketData)) == -1)
    {
        std::cout << "Bad server socket bind to port\n";
        std::cout << strerror(errno) << "\n";
        return -1;
    }

    if(listen(sockfd_, 5) == -1)
    {
        std::cout << "Bad server socket. Marked as passive\n";
        return -1;
    }

    return result;
}

int32_t Server::Init() noexcept
{
    // method for additional initialization if needed.
    return BindSocket2Port();
}

int32_t Server::Run() noexcept
{
    int32_t result{};

    result = Init();
    if(result == -1)
    {
        return result;
    }

    std::cout << "Waiting for new connections...\n";
    connectionSocket_ = accept(sockfd_, nullptr, nullptr);
    if(connectionSocket_ == -1)
    {
        std::cout << "Bad connection from listening socket\n";
        return -1;
    }

    std::cout << "New connection...\n";

    int64_t receivedData;
    char tmpBuffer[2048];

    while(true)
    {
        receivedData = recv(connectionSocket_, tmpBuffer, sizeof(tmpBuffer), 0);
        if (receivedData < 0)
        {
            std::cout << "Bad receiving data from socket\n";
            return -1;
        }

        if (receivedData == 0)
        {
            if(errno == EWOULDBLOCK)
            {
                std::cout << "Data transfer has ended\n";
                break;
            }

            break;
        }

        buffer_.append(tmpBuffer, receivedData);
    }

    if(!FilePacketSerializer::FilePacketIsValid(buffer_))
    {
        std::cout << "Received data is invalid\n";
        std::cout << buffer_ << "\n";
        return -1;
    }

    const auto filename = FilePacketSerializer::GetFilename(buffer_);
    std::cout << "Getting file name: " << filename << "\n";
    std::cout << "Received data[" << buffer_.size() <<   " bytes]: " << buffer_ << "\n";
    const auto newFilename = "ReceivedFromClient_" + filename;
    std::cout << "New file name: " << newFilename << "\n";

    std::ofstream file(newFilename);
    file.write(buffer_.data() + filename.size() + 1, buffer_.size() - filename.size());
    std::cout << "All data has been written into " << newFilename << "\n";
    return result;
}
