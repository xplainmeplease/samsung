#ifndef SAMSUNG_COMMANDLINEOPTIONS_H
#define SAMSUNG_COMMANDLINEOPTIONS_H

#include <cstdint>
#include <string>

class CommandLineOptions
{
public:
    CommandLineOptions() noexcept;
    ~CommandLineOptions() = default;

public:
    [[nodiscard]] bool IsValid(bool isClient) const noexcept;
    [[nodiscard]] int32_t Init(int32_t argc, char** argv) noexcept;
    [[nodiscard]] ExecutionMode GetExecutionMode()  const noexcept;
    [[nodiscard]] std::string   GetIPv4()           const noexcept;
    [[nodiscard]] std::string   GetFilepath()       const noexcept;
    [[nodiscard]] uint16_t      GetPort()           const noexcept;

    static void PrintHelp() noexcept;
private:
    ExecutionMode mode_;
    std::string IPv4_;
    std::string filePath_;
    uint16_t port_;
};


#endif //SAMSUNG_COMMANDLINEOPTIONS_H
