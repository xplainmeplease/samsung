#include <getopt.h>
#include <cstring>

#include "Utilities.h"
#include "CommandLineOptions.h"

CommandLineOptions::CommandLineOptions() noexcept
    : mode_{ExecutionMode::Undefined}, IPv4_{"undefined"}, filePath_{"undefined"}, port_{0}
{
}

int32_t CommandLineOptions::Init(int32_t argc, char ** argv) noexcept
{
    int32_t result {0};

    if(argc < 5)
    {
        std::cout << "[Not enough args.]\n";
        CommandLineOptions::PrintHelp();
        return -1;
    }

    const option longOptions[] = {
            {"mode", required_argument, nullptr , static_cast<int32_t>(OptionID::ExecutionMode) },
            {"ip", required_argument, nullptr , static_cast<int32_t>(OptionID::IPv4) },
            {"port", required_argument, nullptr , static_cast<int32_t>(OptionID::Port) },
            {"file", required_argument, nullptr , static_cast<int32_t>(OptionID::Filepath) },
    };

    int32_t getOptionResult {};
    int32_t optionIndex {};

    while ((getOptionResult = getopt_long(argc, argv, "", longOptions, &optionIndex)) != -1)
    {
        switch (getOptionResult)
        {
            case static_cast<int32_t>(OptionID::ExecutionMode):
            {
                if(strcmp(optarg, "client") == 0)
                {
                    mode_ = ExecutionMode::Client;
                    break;
                }

                if(strcmp(optarg, "server") == 0)
                {
                    mode_ = ExecutionMode::Server;
                    break;
                }

                std::cout << "Incorrect execution mode\n";
                return -1;
            }
            case static_cast<int32_t>(OptionID::IPv4):
            {
                std::cout << "[Option Found: IPv4]\n";
                IPv4_ = std::string(optarg, strlen(optarg));
                break;
            }

            case static_cast<int32_t>(OptionID::Port):
            {
                std::cout << "[Option Found: port]\n";
                port_ = std::strtol(optarg, nullptr,  10);
                break;
            }

            case static_cast<int32_t>(OptionID::Filepath):
            {
                std::cout << "[Option Found: file]\n";
                filePath_ = std::string(optarg, strlen(optarg));
                break;
            }

            default:
                break;

        }
    }

    std::cout << "[" << argc << " options found : " << IPv4_ << " : " << port_ << " : " << filePath_ << "]\n";


    return result;
}

ExecutionMode CommandLineOptions::GetExecutionMode() const noexcept
{
    return mode_;
}

std::string CommandLineOptions::GetIPv4() const noexcept
{
    return IPv4_;
}

std::string CommandLineOptions::GetFilepath() const noexcept
{
    return filePath_;
}

uint16_t CommandLineOptions::GetPort() const noexcept
{
    return port_;
}

bool CommandLineOptions::IsValid(bool isClient) const noexcept
{
    if(isClient)
    {
        return mode_ != ExecutionMode::Undefined && IPv4_ != "undefined" && filePath_ != "undefined" && port_ != 0;
    }
    //server needs only port.
    return mode_ != ExecutionMode::Undefined && port_ != 0;
}

void CommandLineOptions::PrintHelp() noexcept
{
    std::cout << "[Usage]: ./binaryname --mode [client/server] --ip [IPv4] --port [port] --file [filename]";
}
