# samsung test task

How to build:
```
cmake .
cmake --build .
```

How to start:
```
General:
./binaryname --mode [client/server] --ip [IPv4] --port [port] --file [filename]

For client:
./samsung --mode client --ip 192.168.XXX.XXX --port 59999 --file yourfile.txt

For server:
./samsung --mode client --port 59999 
```
